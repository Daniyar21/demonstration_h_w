import logo from './logo.svg';
import './App.css';
import {useState} from "react";
import Modal from "./Modal/Modal";

function App() {

    const [show, setShow] = useState(false);

    const modalHandler = () => {
      setShow(true);
    };

    const cancelHandler = () => {
        setShow(false);
    };

  return (
    <div className="App">
        <Modal
            show = {show}
            title = 'Some kinda modal title'
            cancel = {cancelHandler}

        />
      <button onClick={modalHandler}>Показать</button>
    </div>
  );
}

export default App;
