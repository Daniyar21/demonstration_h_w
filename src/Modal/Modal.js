import React from 'react';
import './Modal.css';

const Modal = props => {
    return (
            <div className="Modal"
            style={{
                transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                opacity: props.show ? '1' : '0',
        }}
        >
                <h1>{props.title}</h1>
                <button style={{
                    position: "absolute",
                    top: "0",
                    right: "0"
                }
                }onClick={props.cancel}>X</button>

            {props.children}

                <p>This is modal context</p>
            </div>
    );
};

export default Modal;